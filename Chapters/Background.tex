% Chapter 2 - Background

\chapter{Background}
\label{chap:background}

Here is the background necessary to understand this thesis, starting from the basic concepts such as Cyber-Physical
Systems to standards like \texttt{AutomationML}.

\section{Basics}
\subsection{The Fourth Industrial Revolution}
With the development of information technology after the World War II, the use of computers and robots within the
manufacturing industry was made possible.
The use of these technologies led to what today is defined as the \emph{Third Industrial Revolution}, which brought
automation of production chains. The concept of mass production though is still the same introduced by Ford, so the
final products that come out of the chain are all the same, and if I want to build something similar but customized,
manual work is needed or mass change of the machines themselves.

The arrival of the \emph{Internet} though opened a lot of new possibilities. One of the first consequences of its
introduction was a complete revolution of communication methods, with emails and other instant services.
Further development of Internet, in parallel with the ever improving miniaturization of computers, led to the idea of
interconnected machines for realizing \emph{smart manufacturing}.

In 2011 the German government, after all of these progresses, proposed a national plan of investments under the term
\emph{``Industry 4.0''} which promotes the heavy computerization of manufacturing.
The general goal for the project is strong product customization under the condition of highly flexible
mass-production. Methods of machine self-configuration, self-optimization, self-diagnosis and co-operation have been
introduced in years and are extensively used in this scenario.

Technically speaking, all of this can be achieved with the union of Cyber-Physical Systems \parencite{Centomo2019},
Industrial Internet of Things \parencite{Vachalek2017} and Internet of Services.

\subsection{Cyber-Physical Systems}
\begin{figure}
	\centering
	\includegraphics[width=1\linewidth]{Figures/CPS}
	\caption{Structure of a CPS}
	\label{fig:cps}
\end{figure}
The word ``\emph{Cyber-Physical Systems}'' (CPS) refers to systems composed of a \emph{Cyber} part, which is usually an
embedded system, so with digital hardware and dedicated software, and a \emph{Physical} part, which is the actual
physical process to control, as shown in \cref{fig:cps}.
CPS \parencite{Centomo2019} deeply integrate software with real, physical processes, and they involve multidisciplinary
approaches, merging theory of cybernetics, mechatronics, design and process science.

In fact, development of CPS poses serious challenges, starting from the large differences in the design practice
between the various engineering disciplines involved, such as software and mechanical engineering.
Additionally, as of today there is no ``language'' in terms of design practice that is common to all the involved
disciplines in CPS, though \emph{AutomationML} \parencite{Drath2008} is trying to emerge as a standard in this field,
accompanied by the \emph{Functional Mock-Up Interface} \parencite{Blockwitz2012} for simulation and co-simulation needs.

\subsection{Digital Twins}
The idea of \emph{Digital Twins} came before the concept of \emph{Industry 4.0}, originally delineated by Grieves
\parencite{Grieves2014} ten years before.
A digital twin is a virtual replica of the production plant, which can be simulated in advance or in real-time.
Obviously, we can simulate the plant at various levels: we could want a detailed simulation that uses complete machine
models, or we could just want to simulate arbitrary steps, like raw materials becoming a semi-finished product than
final product.

\newpage
\section{Technologies}
\subsection{OPC UA Communication Protocol}
\emph{OPC Unified Architecture} (\texttt{OPC UA}, \cite{Mahnke2009}) aims to standardize \emph{Machine to Machine}
(M2M) communication \parencite{VID/VDE2015}.

Definition of \texttt{OPC} specifications \parencite{OPCUAspec} started to simplify and to standardize data
exchange between software applications in industrial environment. The rapid diffusion of the first version of
\texttt{OPC} specifications was due to the choice of Microsoft’s \texttt{DCOM} as the technological basis.
However, exactly this point raised the majority of criticism regarding \texttt{OPC} because it was too focused on
Microsoft, platform-dependent and not firewall-capable, and thus not suitable for use in cross-domain scenarios and for
the Internet. When \texttt{XML} and Web Services technologies have become available, the OPC Foundation adopted them as
an opportunity to eliminate the shortcomings of \texttt{DCOM}.
Since 2003 the \emph{OPC XML Data Access} (\texttt{DA}) specification has offered a first service-oriented
architectural approach besides the ``classic'' \texttt{DCOM}-based \texttt{OPC} technology. This Web services-based
concept enabled applications to communicate independently of the manufacturer and platform.

Few years later, the OPC Foundation has introduced the \texttt{OPC UA} standard which is based on a
\emph{service-oriented}, \emph{technology} and \emph{platform-independent} approach, creating new and easy
possibilities of communicating with Linux/Unix systems or embedded controls on other platforms and for implementing
\texttt{OPC} connections over the Internet. The new possibilities of using \texttt{OPC} components on non-Windows
platforms, embedding them in devices or implementing a standardized \texttt{OPC} communication across firewall
boundaries allow speaking of a change of paradigms in \texttt{OPC} technology. \texttt{OPC UA} servers can be varied
and scaled in their scope of functions, size, performance and the platforms they support. For embedded systems with
limited memory capacities, slim \texttt{OPC UA} servers with a small set of \texttt{UA} services can be implemented; at
the company level, in contrast, where memory resources are not that important, very powerful \texttt{OPC UA} servers
can be used with the full functionality.

\texttt{OPC UA} specifications now offer a security model, which wasn't available in the previous versions of
\texttt{OPC} specifications; the \texttt{OPC UA} security governs the authentication of clients and servers and ensures
data integrity, trustworthiness and authorization within \texttt{OPC} communication relationships
\parencite{OPCUAspec_security}.

The \texttt{OPC UA} architecture models Clients and Servers as interacting partners. Each system may contain multiple
Clients and Servers. Each Client may interact concurrently with one or more Servers, and each Server may interact
concurrently with one or more Clients. An application may combine Server and Client components to allow interaction with
other Servers and Clients. \emph{Server to Server} \parencite{OPCUAspec_overview} interactions in the Client Server
model are interactions in which one Server acts as a Client of another Server. Server to Server interactions allow for
the development of servers that:
\begin{itemize}
	\item exchange information with each other on a peer-to-peer basis, this could include for example redundancy;
	\item are chained in a layered architecture of Servers to provide aggregation of data from lower-layer Servers,
	higher-layer data constructs to Clients, concentration interfaces to Clients for single points of access to
	multiple underlying Servers.
\end{itemize}
\texttt{OPC UA} can be used at different levels of the automation pyramid for different applications within the same
environment. At the plant floor level, an \texttt{OPC UA} server may run in a controller providing data from field
devices to \texttt{OPC UA} clients (e.g. HMIs, SCADA). On top of the plant floor at operation level, an \texttt{OPC UA}
application may be a client collecting data from the server at the lower level, performing special calculations and
generating alarms; an example is represented by an \texttt{OPC UA} client integrated in an ERP system, obtaining
information about used devices in the plant floor (e.g. working hours) and creating a maintenance request.
\begin{figure}
	\centering
	\includegraphics[width=1\linewidth]{Figures/OPCUA}
	\caption{OPC UA stack example}
	\label{fig:opcua}
\end{figure}

\subsubsection{Used Technology}
The \texttt{OPC UA} standard is defined in an abstract manner. This enables the standard to remain relevant, even if
technologies widely in use change. The tasks required to be accomplished by the technologies chosen are data encoding,
data security, and data transport. A set of technology that implements this set of tasks is called a \emph{stack}. The
OPC Foundation provides stacks for several languages. Data Encoding is done in \texttt{OPC UA} Binary or \texttt{XML}.

The standard stack implements network communication over \texttt{TCP}. \texttt{SOAP} over \texttt{HTTP}, is also
supported, but in practice it is rarely used. Several companies also produce Software Development Kits (SDKs), which
include the stack, and more importantly, implementations of the features in \texttt{OPC UA}. This enables developers to
develop applications with ease. In practice using an SDK (commercial or free) is essential for utilizing the full
extent of \texttt{OPC UA}, as implementing these features independently is a major task.

\subsubsection{Server Address space}
In an \texttt{OPC UA} server is contained the information model \parencite{OPCUAspec_information}, in which all
information regarding a machine is stored. This model is a graph data structure and can be used to represent a wide
variety of structured information.
Concepts of object-oriented programming, such as type hierarchies and inheritance are also incorporated into
\texttt{OPC UA}.

The totality of the data an \texttt{OPC UA} server exposes is therefore called \emph{address space}. Being a graph, it
is made by nodes and edges:
\begin{itemize}
	\item \textbf{Nodes} are identified by their \emph{NodeId} and are divided into \emph{NodeClasses}. There are eight
	different kinds of \emph{NodeClasses}: \texttt{Objects}, \texttt{Variables}, \texttt{Methods}, \texttt{View} which
	are instances of the types \texttt{ObjectTypes}, \texttt{VariableTypes}, and \texttt{MethodTypes}.

	\item \textbf{Edges} called \emph{References}
\end{itemize}
Excepting certain nodes that must be present on every \texttt{OPC UA} server, there is no predefined way to arrange the
\texttt{OPC UA} address space. This enables each implementer of an \texttt{OPC UA} server to arrange the address space
according to their own needs.

\paragraph{NodeIds and Namespaces}
Each Node is identified with a unique \emph{NodeId}. These identifiers can be of three types, \emph{Numeric},
\emph{String} or \emph{GUID}. They are unique numbers, unique strings, and unique 64 bit numbers, respectively.
Identifying numbers and strings need to be chosen in a way that does not cause a conflict. A GUID is generated
randomly, yet its uniqueness is in practice statistically ensured as it is such a large number. Each \texttt{NodeId}
has a \texttt{Namespace} string, which further differentiates it from other \texttt{NodeIds}. The same Node Identifier
can be used in different \texttt{NodeIds} on the condition they are separated by \texttt{Namespace}. In order to avoid
redundant use of memory during runtime, nodes do not contain the \texttt{Namespace} string itself, but rather a
\texttt{NameSpaceIndex}, referring to the \texttt{Namespace}. The \texttt{NameSpaceIndex} can be converted to the
\texttt{Namespace} via a \texttt{Namespace} table.

\paragraph{Attributes}
Each \emph{Node} contains a set of \emph{Attributes}. The exact attributes of a node depend on its \texttt{NodeClass},
but some attributes are common to all nodes. An \texttt{Attribute} consists of its name and value in a certain
\texttt{DataType}. The \texttt{DataType} indicates how the value should be interpreted. \texttt{Strings},
\texttt{Integers}, and \texttt{Date-Times} are some common \texttt{DataValues}. Binary data can also be saved under the
\texttt{DataType} \texttt{ByteString}, this enables arbitrary, and even large data to be contained in the address
space. Some Attributes are obligatory for all Nodes: \texttt{NodeId}, \texttt{NodeClass}, \texttt{BrowseName}, and
\texttt{DisplayName}. These are used to identify, categorize, and name the Node. All the values related directly to the
node itself are encoded as Attributes.

\paragraph{References and ReferenceTypes}
An \texttt{OPC UA} address space is effectively a graph, and references are its edges. \emph{References} form the
structure and relations of the address space. As with graph edges, references can be directed or undirected. Each
\texttt{Reference} has a \texttt{ReferenceType}, represented as a \texttt{Node} in the address space. In practice, the
most important distinction between different \texttt{ReferenceTypes} is that some are hierarchical, and some are not.
References that have a hierarchical \texttt{ReferenceType} may not contain cycles, whereas references with a
non-hierarchical \texttt{ReferenceType} can form any graph. Several networks can be formed in the same address space by
using different \texttt{ReferenceTypes} to differentiate between them.

\paragraph{Objects and ObjectTypes}
An \emph{Object} is a Node that is used primarily to organize the \emph{Address Space} in whichever way desired. It
does not contain data, other than to describe the Object itself. A common \texttt{ObjectType} is \texttt{Folder}, used
to group nodes hierarchically. \texttt{ObjectTypes} are used as metadata for external applications, as well as to
restrict the configuration of an Object through \texttt{ModelingRules}.

\paragraph{Variables}
\emph{Variable Nodes} are used to contain values. Each value has a \texttt{DataType}, depending on the Variable. A
simple variable consists of a single node, the \emph{Value Attribute} of which contains the current value of the node.
Complex variables can contain child variables, and they can subsequently reference child variables down to an arbitrary
complexity.

\paragraph{Events}
Events are occurrences at a specific point of time, which are received via \emph{Subscriptions}. Events can be, for
example, notifications of occurrences in the underlying system, errors, or address space configuration changes. Events
are generated by a node specified as an \texttt{EventNotifier}, which in turn propagates events according to
\texttt{HasEventNotifier} references. \texttt{Events} have an \texttt{EventType}, which can be used to categorize and
filter them. One important use of events is notifications and alarm events generated by \texttt{Alarms} \&
\texttt{Conditions}, which are discussed in more depth below.

\paragraph{Browse}
The address space of every \texttt{OPC UA} server is required to contain certain nodes, such as the \texttt{Server}
object, and the \texttt{Objects} folder. These, among others, provide an entry point into the address space. In order
to discover the address space parented by these entry points, the references they contain need to be followed. Provided
a node, Browse will return the references it holds. They can then further be browsed to reveal the rest of the address
space. A noteworthy service related to node discovery is \texttt{Query}, which returns nodes across the entire Address
Space according to user-defined criteria. However, it is not always supported by \texttt{OPC UA} server
implementations, and Browse must sometimes be used instead.

\paragraph{Read and Write}
Once a node in the address space has been as been identified by \texttt{NodeId}, its Attributes can be accessed using
the \emph{Read} and \emph{Write} services. These services are always used when accessing node \texttt{Attributes}, and
their names are self-explanatory. Read and Write will trigger value change notifications for \texttt{Nodes} with
\texttt{Subscriptions}.

\paragraph{Subscription}
Clients can Subscribe to three different types of data changes on an \texttt{OPC UA} server: variables, events, and
aggregated values. Each data source is represented by a \texttt{MonitoredItem}. Monitored items has some important
parameters:
\begin{itemize}
	\item \textbf{Sampling interval} period at which the OPC UA server checks for new values, which are generated at a
	machine-dependent (synchronous or not) rate.

	\item \textbf{Queue size} configurable size of the queue that stores data. By default, only one data change is
	queued. For events, the default behavior is to queue as much event data	as possible according to server limits.
\end{itemize}
One or more monitored items are combined into a single \texttt{Subscription} item. The subscription keeps track of the
monitored items and delivers information on data changes with the \texttt{Publish} service according to a
\texttt{Publish} Interval. A filter can be introduced for each type of monitored item. It can also be conditioned on
the type of change, for example when the status of the value changes. Event filtering can be even more complex.
Aggregate data filtering (not to be confused with server aggregation) is a method of sampling time series for averages
and the like for set segments of time.

\paragraph{Historical data}
Historical data refers to a time series of changes in the \texttt{Attribute} value of a certain \texttt{Variable}.
Variables contain the \texttt{Historizing} attribute, which indicates whether historical data is currently being saved.
The \texttt{HistoryRead} service provides access to the time series. Historical data is typically implemented outside
the SDK. Historical data can also be aggregated, meaning an average or other time series analysis can be performed on
it by the server.

\subsubsection{OPC UA SDK and software}
\paragraph{Open62541} It is an open source implementation of \texttt{OPC UA}, distributed under the \texttt{Mozilla
Public License v2.0}, which allows the SDK to be used in closed source projects. It recently received the official OPC
Foundation certification, though not completely.
It's written in pure \texttt{C99} and it's designed to use very few computational resources, making it suitable for
being used in embedded devices.

\paragraph{Unified Automation} It's a commercial \texttt{OPC UA} SDK written in \texttt{C++} and it's one of the best
available also for the provided support and documentation.

\paragraph{Client tools} Unified Automation provides a free generic client, \texttt{UaExpert}, which has some nice
built-in features useful for debugging purposes.

Another interesting client is \texttt{Prosys OPC UA} client, written with \texttt{JavaFX}. It has fewer features than
\texttt{UaExpert}, but is more intuitive to use. It also comes with easy variable monitoring and graphing features.

\subsection{AutomationML}
\texttt{AutomationML} (\texttt{AML}) is a standard based on \texttt{XML} which aims to provide reliable data exchange
in the engineering process of production systems \parencite{Drath2008}. An interesting aspect of \texttt{AML} is that
it doesn't develop any new data format for achieving its purpose, but instead it uses already existing formats, adapted
and extended when needed, then merged properly.
So far, the representation of plant specific data in general and in special the plant structure, geometry and
kinematics, and logic description is possible. Additional representations for networks, mechatronics systems, and
others are in progress.
Within \texttt{IEC62714} all parts of \texttt{AML} are going to be standardized internationally.
\begin{figure}[h]
    \centering
    \includegraphics[width=1\linewidth]{Figures/AML}
    \caption{AutomationML document structure.}
    \label{fig:aml}
\end{figure}
\texttt{AutomationML} has a lean and distributed file architecture. It does not define any new file format but combines
existing established \texttt{XML} data formats which have been proven in use for their specific domain. This is why the
normative part of the \texttt{IEC62714-1:2018} document consists of $32$ pages only. The data formats for the following
modelling domains are:
\begin{itemize}
	\item \emph{object topologies} including hierarchies, properties and relations of objects: \texttt{CAEX} according
	to \texttt{IEC 62424}
	\item \emph{geometries} and \emph{kinematics} of objects: \texttt{COLLADA 1.4.1} and \texttt{1.5.0}
	(\texttt{ISO/PAS\\17506:2012})
	\item \emph{discrete behavior} of objects: \texttt{PLCopen XML 2.0} and \texttt{2.0.1}; in addition,\\
	\texttt{IEC62714-4} will allow the usage of	\texttt{IEC61131-10}
\end{itemize}
\texttt{CAEX} according to \texttt{IEC62424} forms the base of \texttt{AutomationML}. It stores object-oriented
engineering information, e.g. a plant hierarchy structure (see \texttt{AutomationML} topology). Each \texttt{CAEX}
object can contain properties and reference geometry, kinematics or logics information stored in third party
\texttt{XML} files. This enables cross-domain modelling and is designed for future extension (\cref{fig:aml}).

\subsubsection{Topologies}
Object hierarchies in \texttt{CAEX} form the core of \texttt{AutomationML} (\cref{fig:amlhierarchy}). A \texttt{CAEX}
object is a data representation of any asset. It can model physical assets, e.g. a motor, a robot, a tank; or abstract
assets like a function block, a model or a folder. \texttt{CAEX} allows to link those objects to systems, since every
physical or logical system is characterized by internal elements (objects) which may contain further internal elements,
and all elements may have interfaces, attributes and connections with each other. Finally, \texttt{CAEX} allows the
modeling of any plant topology, communication topology, process topology, resource topology etc.
\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\linewidth]{Figures/AMLhierarchy}
	\caption{Example of a plant topology in AML.}
	\label{fig:amlhierarchy}
\end{figure}

\subsubsection{Geometry and kinematics}
As mentioned above \texttt{AutomationML} exploits the international standard \texttt{COLLADA 1.4.1} and \texttt{1.5.0}
for the representation of geometry and kinematics information which is standardized as \texttt{ISO/PAS 17506:2012}.
Therefore, \texttt{AutomationML} has developed a two-stage process:
\begin{enumerate}
	\item relevant geometries and kinematics are modelled as \texttt{COLLADA} files.
	\item these files and the data objects within them are referenced out of the \texttt{CAEX} file
	(\cref{fig:amlcollada}).
\end{enumerate}
\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{Figures/AMLcollada}
	\caption{Referencing COLLADA subdocuments.}
	\label{fig:amlcollada}
\end{figure}

\texttt{COLLADA} stands for \texttt{COLLAborative Design Activity}. It was developed by the KHRONOS association under
the leadership of Sony as an intermediate format within the scope of digital content creation in the gaming industry.

It is designed to enable the representation of 3D objects within 3D scenes covering all relevant visual, kinematic, and
dynamic properties needed for object animation and simulation.
\texttt{COLLADA} is an \texttt{XML}-based data format
with a modular structure enabling the definition of libraries of visual and kinematic elements. It can contain
libraries for the representation of geometries, materials, lights, cameras, visual scenes, kinematic models, kinematic
scenes, and others.

The most important feature of \texttt{AutomationML} is the clear identifiability of objects in \texttt{COLLADA} files,
which allows the integration of these files into \texttt{AutomationML}. Several data objects within a \texttt{COLLADA}
file have a unique identification (ID) like geometries, visual scenes, kinematic models and kinematic scenes.

In order to reference these objects, \texttt{AutomationML} has defined a special interface class within the
\texttt{AutomationMLInterfaceClassLib} named \texttt{COLLADAInterface} which shall be applied to derive the needed
interfaces for geometry integration.
This interface class itself is derived from the interface class \texttt{ExternalDataConnector} and therefore has an
attribute \texttt{refURI}. This attribute can be used to reference into a \texttt{COLLADA} file, thereby referring to
an ID of an object modelled in the \texttt{COLLADA} file.\\
Thus, the value of the \texttt{refURI} attribute shall contain a string structured like\\
\texttt{file:///filename.dae\#ID}. The attribute \texttt{refType} is used to differentiate between various ways of
embedding objects in a modeled scene. It can provide information on how static an object in the scene is in relation to
other objects, e.g. whether a work piece and the conveyor belt move at the same time.

\subsubsection{Modeling behaviour}
\texttt{AutomationML} with its object-centric modelling approach enables a dedicated storing of logic information on
object level. For this purpose, certain object semantics as well as object interface semantics are developed. In
addition to that, logic models are identified, commonly used in the engineering process of a production system, and
made exchangeable, but also transformable among each other. This allows and supports an information enrichment process
in terms of logic information that is required for the scope of \texttt{AutomationML}. All concepts are going to be
standardized in \texttt{IEC 62714-4}.

Logic information is an important aspect for raw system planning, electrical design, \texttt{HMI} development,
\texttt{PLC} and robot control programming, for simulation purposes, and virtual commissioning. To support the
different phases in the iterative production system engineering process covering different levels of detail,
\texttt{AutomationML} needs to be able to store logic information from different tools and disciplines.

\paragraph{Types of logic information}
While \emph{sequencing information} describes the instructions to the controlled object, \emph{behavior information}
describes the possible responses of the controlled object to the sequencing information and to other external
interactions. The third type is \emph{interlocking information}.
This describes the instructions to the controlled object to avoid the object from causing harmful effects on humans and
environment. Therefore, an object of the production system structure can have up to three types of logic information
assigned to it since an object can be composed of other objects. These objects can then be composed of other objects
and so on.

Logic information is generally organized and stored in logic models. To be applicable within the engineering process of
production systems, \texttt{AutomationML} supports common logic models which cover different phases of the engineering
process. A common basis named \texttt{Intermediate Modeling Layer} (IML) is defined for the following three logic
models. With this, it is possible to transform one logic model into another one. A forward transformation supports the
information enrichment process and reduces or avoids a re-entry of information between the exchanging engineering
tools. Theoretically, this workflow can be carried out until an executable program. But the backwards transformation is
also reasonable as the engineering process is not a linear process. It has iterations and cycles due to possible
requirements, changes or errors made in engineering. However, since the logic models have different modeling powers,
backward transformation may result in information loss when not handled appropriately by the importers and exporters of
software tools.

For the interlocking information, \texttt{AutomationML} provides a concept for how cause and effect matrices (logic
model) can be stored using \texttt{FBD}.

\subsection{Functional Mockup Interface}
The \emph{Functional Mockup Interface} \parencite{FMI} standard defines an interface that allows to encapsulate models
from different tools.
The primary goal is to support the exchange of simulation models between suppliers and OEMs even if a large
variety of different tools are used. Actually the \texttt{2.0} version of the standard has been released, and it
consists of two main parts: Model Exchange and Co-Simulation.

The \emph{Model Exchange} interface provides a method to generate C-code in the form of an input output block. This
method is used by different simulators to export only the descriptions of their models without exposing their internal
solver algorithm which, in general, is closed source.

The \emph{Co-Simulation} interface provides a method to export a model in the form of a block including also a
mathematical solver needed to execute correctly that model. This allows a simulator to load and execute
other models correctly even when the correct solver is not available. The provided model description is very similar to
the \emph{Model Exchange} one. The main difference between the two approaches is the location of the solver: in the
first case it is provided by the simulator tool while in the second is integrated into the model exported.

\subsubsection{Functional Mockup Unit}
A component which implements the interface is called \emph{Functional Mockup Unit} \texttt{FMU}. As of version
\texttt{2.0} a single \texttt{FMU} can implement both interfaces. An \texttt{FMU} is a zip file and it contains
different informations.

\paragraph{XML description}
An \texttt{XML} file contains the definition of all variables of the \texttt{FMU} that are exposed to the environment
in which the \texttt{FMU} shall be used. Each port of the interface has different properties like name, causality
(e.g., input, output, parameter, etc), a type and a value reference. The value reference of a port represents a numeric
identifier that must be unique, since it will be used from the external tools to address it. In a \emph{Co-Simulation}
environment this file can also specify all the needed information about the internal solver like a set of capability
flags. In a \emph{Model Exchange} context all the model equations are provided with a small set of C functions. This
information is used by the external simulator to instantiate its internal solver.

\paragraph{Dynamic libraries}
An \texttt{FMU} can provide one or more dynamic libraries according to its type. For example a \emph{Co-Simulation}
\texttt{FMU} must provide an already built shared library ready to be loaded. This library must implement the
functionality through a set of functions defined by the standard. The most relevant functions are:
\begin{itemize}
    \item \texttt{fmi2SetupExperiment} initializes the experiment
    \item \texttt{fmi2Set} sets the value of an input port of the \texttt{FMU}
    \item \texttt{fmi2Get} gets the value of an output port of the \texttt{FMU}
    \item \texttt{fmi2DoStep} executes the model contained in the \texttt{FMU}
\end{itemize}

\subsection{MATLAB/Simulink}
\texttt{MATLAB} is a multi-paradigm numerical computing environment, featuring a proprietary scripting language
developed by MathWorks. \texttt{MATLAB} allows matrix manipulations, plotting of functions and data,
implementation of algorithms, creation of user interfaces, and interfacing with programs written in other languages.
The environment is highly configurable and extensible with plugins, and in fact one of the most popular one is
Simulink, which frequently comes directly with the default \texttt{MATLAB} installation.
\texttt{Simulink} adds graphical multi-domain simulation and model-based design for dynamic and Embedded Systems.
In Model-Based Design, a system model is at the center of the workflow. Model-Based Design enables fast
and cost-effective development of dynamic systems, including control systems, signal processing systems, and
communications systems.
Model-Based Design allows you to:
\begin{itemize}
    \item use a common design environment across project teams
    \item link designs directly to requirements
    \item refine algorithms through multi-domain simulation
    \item automatically generate embedded software code and documentation
\end{itemize}

\subsubsection{Simulink compiler and FMU export tool}
Till version \texttt{2019b Simulink} had a plugin, called ``Tool-Coupling Co-Simulation FMU Export'', that allowed to
generate an FMU from a model, supporting the Co-Simulation technique. The problem is that this approach required to
have a local \texttt{MATLAB} installation to effectively simulate the model, and it wasn't a robust approach.
Starting from \texttt{MATLAB 2020a}, Simulink now comes with a handy feature that allows the model to be actually
compiled in a standalone simulable FMU, without requiring an external \texttt{MATLAB} instance. There are some caveats,
as the model has to use a fixed time-step, but at least the simulation is hassle-free.


\subsection{Tecnomatix Plant Simulation}
\texttt{Tecnomatix Plant Simulation} is a computer application developed by \emph{Siemens PLM Software} for modeling,
simulating, analyzing, visualizing and optimizing production systems and processes, the flow of materials and logistic
operations. By using \texttt{Tecnomatix Plant Simulation}, users can optimize material flow, resource utilization and
logistics for all levels of plant planning from global production facilities, through local plants, to specific lines.
\texttt{Tecnomatix Plant Simulation} belongs to the \emph{Product Lifecycle Management Software} (PLM) portfolio.
\texttt{Plant Simulation} is a \emph{Material Flow simulation} or \emph{Discrete Event Simulation} (DES) Software.
\emph{Material flow} refers to the description of the transportation of raw materials, pre-fabricates, parts,
components, integrated objects and final products as a flow of entities. This means that a computer model allows to
execute experiments and to run through ``what if scenarios'' without either having to experiment with the real
production environment or, when applied within the planning phase, long before the real system exists.