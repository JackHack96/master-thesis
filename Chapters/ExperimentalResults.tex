% Chapter 7 - Experimental Results

\chapter{Experimental results}
\label{chap:experimentalresults}

In this chapter the result of the model generation flow is integrated first into a simple model and then into the
\emph{Digital Twin} of the ICE lab, an educational lab of University of Verona for demoing Industry 4.0 concepts and
research.
Both the examples are made using \texttt{Tecnomatix Plant Simulation}.

The model which gets integrated into both project is the \emph{Two Speed Transmission}
(\cref{fig:twospeedtransmission}) model by \emph{Mathworks}, which comes as a default example with \texttt{MATLAB}.
It was first mapped to \texttt{AutomationML} using the \texttt{SimulinkRoleClassLib}, then parsed with
\texttt{amlparser} and the resulting script was run inside \texttt{MATLAB}, thus generating an \texttt{FMU}.
This \texttt{FMU} is then hosted by running \texttt{FMU2OPCUA}, as shown in \cref{fig:fmu2opcuausage}.

\section{Simple producer-consumer}
The simple model (\cref{fig:simplemodel}) is composed of a \emph{Source} which produces \emph{Mobile Units} (MUs) in a
certain amount, a \emph{Station} which represents a generic processing step and a \emph{Drain} which consumes the
processed unit.
\begin{figure}[h]
\centering
\includegraphics[width=0.9\linewidth]{Figures/SimpleModel}
\caption{The simple model scheme.}
\label{fig:simplemodel}
\end{figure}

Plant Simulation offers an integrated OPC UA client (the one on the top right of \cref{fig:simplemodel}) which is
fairly simple to use. Once added to the model, its dialog window asks for the IP address and the port
(\cref{fig:plantopcuaclient}).
\begin{figure}[h]
\centering
\includegraphics[width=0.7\linewidth]{Figures/PlantOPCUAClient}
\caption{OPC UA client main dialog.}
\label{fig:plantopcuaclient}
\end{figure}
Global model variables can be directly connected to the ones exposed by the server through the \texttt{Items} dialog as
shown below in \cref{fig:plantopcuaclientitems}.
\begin{figure}[h]
\centering
\includegraphics[width=1\linewidth]{Figures/PlantOPCUAClientItems}
\caption{Model variable - Server variable connections.}
\label{fig:plantopcuaclientitems}
\end{figure}
By doing this way, whenever the server variables' values change, the Plant Simulation variables written in the
far-right column gets updated with the same value automatically.

On the \emph{Station} block in the middle, a method is added on the exit control section, which means that whenever an
MU exits the machine the method is going to get called.
As the \texttt{FMU2OPCUA} program supports different simulation modes, one can either choose to produce a predefined
input signals set based on time and use the \texttt{SimulationTime} exposed variable to change signal values
accordingly, or just rely upon the default stop experiment time and just set values in that restricted
time slice.
There's also another way which was the chosen one, which is the manual mode. In this mode the simulation gets
controlled by the client through the \texttt{DoStep} port, as shown in \cref{code:onExitStation}.
The code below is for reproducing the signals shown in \cref{fig:signalbuilder}.
\begin{figure}[h]
\centering
\includegraphics[width=1\linewidth]{Figures/SignalBuilder}
\caption{The Brake and the Gear signals for the Station.}
\label{fig:signalbuilder}
\end{figure}
\begin{codeInput}{basic}{The onExit() method of the Station}{onExitStation}
OPCUA.setItemValue("Restart",true);
OPCUA.setItemValue("Simulation Step",1.0)

OPCUA.setItemValue("Gear",0.00);
OPCUA.setItemValue("Brake",0.00);
OPCUA.setItemValue("DoStep",true);
stopuntil SimulationTime = 1.0 prio 1

OPCUA.setItemValue("Gear",1.00);
OPCUA.setItemValue("Brake",0.00);
OPCUA.setItemValue("DoStep",true);
stopuntil SimulationTime = 2.0 prio 1

OPCUA.setItemValue("Gear",1.00);
OPCUA.setItemValue("Brake",0.00);
OPCUA.setItemValue("DoStep",true);
stopuntil SimulationTime = 3.0 prio 1

OPCUA.setItemValue("Gear",0.00);
OPCUA.setItemValue("Brake",0.50);
OPCUA.setItemValue("DoStep",true);
stopuntil SimulationTime = 4.0 prio 1

OPCUA.setItemValue("Gear",0.00);
OPCUA.setItemValue("Brake",0.00);
OPCUA.setItemValue("DoStep",true);
stopuntil SimulationTime = 5.0 prio 1

OPCUA.setItemValue("Gear",0.00);
OPCUA.setItemValue("Brake",0.00);
OPCUA.setItemValue("DoStep",true);
stopuntil SimulationTime = 6.0 prio 1

OPCUA.setItemValue("Gear",0.00);
OPCUA.setItemValue("Brake",0.00);
OPCUA.setItemValue("DoStep",true);
stopuntil SimulationTime = 7.0 prio 1

OPCUA.setItemValue("Gear",0.00);
OPCUA.setItemValue("Brake",0.00);
OPCUA.setItemValue("DoStep",true);
stopuntil SimulationTime = 8.0 prio 1

OPCUA.setItemValue("Gear",0.00);
OPCUA.setItemValue("Brake",0.00);
OPCUA.setItemValue("DoStep",true);
stopuntil SimulationTime = 9.0 prio 1

OPCUA.setItemValue("Gear",0.00);
OPCUA.setItemValue("Brake",10.00);
OPCUA.setItemValue("DoStep",true);
stopuntil SimulationTime = 10.0 prio 1

stopuntil SpeedOutput = 0.0 prio 1

@.move
\end{codeInput}

In this case, the response is quiet, as the maximum gear value reached is $1$ and the brake gets up till $0.5$, so when
the simulation time is $10$ the \texttt{SpeedOutput} will already be $0$.
The result is that for every MU produced by the source, $10$ seconds will pass when entering the station, regardless of
the simulation speed.

\newpage
\section{The ICE lab model}
\begin{figure}[h]
\centering
\includegraphics[width=1\linewidth]{Figures/ICE}
\caption{The ICE lab Digital Twin.}
\label{fig:ice}
\end{figure}
\noindent
The ICE lab model is quite complicated as shown in \cref{fig:ice}. It's composed of many parts:
\begin{itemize}
\item A vertical storage
\item The conveyor belt system and a mini pallet
\item An electronic control panel
\item A milling machine
\item Two 3D printers
\item An assembly station
\item A robotic vision system
\end{itemize}
This digital twin simulates the following process:
\begin{enumerate}
\item The MUs stored in the vertical storage are taken on the mini pallet
\item When the mini pallet reaches the stations, it gets processed
\item The milling station is where the \texttt{FMU} has been inserted
\end{enumerate}
The \emph{Two Speed Transmission} model can be used to mimic the processing time of the milling machine.
Different ``recipes'' can be used, each one representing a different signal set, selectable from a menu on the top
right corner.
This is useful for simulating the different effects that different input signal sets can have on the processing times.
Four recipes were made, represented below in \cref{fig:recipesinput}.
This recipes produced the output showed in \cref{fig:recipesoutput}
\begin{figure}[H]
    \begin{minipage}[b]{0.5\linewidth}
        \centering
        \includegraphics[width=1\linewidth]{Figures/Recipe1.png}
    \end{minipage}
    \begin{minipage}[b]{0.5\linewidth}
        \centering
        \includegraphics[width=1\linewidth]{Figures/Recipe2.png}
    \end{minipage}
    \begin{minipage}[b]{0.5\linewidth}
        \centering
        \includegraphics[width=1\linewidth]{Figures/Recipe3.png}
    \end{minipage}
    \begin{minipage}[b]{0.5\linewidth}
        \centering
        \includegraphics[width=1\linewidth]{Figures/Recipe4.png}
    \end{minipage}
    \label{fig:recipesinput}
    \caption{The four recipes input signals}
\end{figure}
\begin{figure}[H]
    \begin{minipage}[b]{0.5\linewidth}
        \centering
        \includegraphics[width=1\linewidth]{Figures/Recipe1.pdf}
    \end{minipage}
    \begin{minipage}[b]{0.5\linewidth}
        \centering
        \includegraphics[width=1\linewidth]{Figures/Recipe2.pdf}
    \end{minipage}
    \begin{minipage}[b]{0.5\linewidth}
        \centering
        \includegraphics[width=1\linewidth]{Figures/Recipe3.pdf}
    \end{minipage}
    \begin{minipage}[b]{0.5\linewidth}
        \centering
        \includegraphics[width=1\linewidth]{Figures/Recipe4.pdf}
    \end{minipage}
    \label{fig:recipesoutput}
    \caption{The four recipes output}
\end{figure}
\newpage
It can be observed that when the gear reaches value $2$, the speed output goes up to $250-300$, while in the first recipe it doesn't even go beyond $30$. In the first recipe the gear has a maximum value of $1$ and it lasts 2 seconds, while the brake is also more powerful reaching $0.50$.
In the other recipes the relation between the speed and the brake intensity and duration can be easily observable. The As the brake gets $0$ after the 5th second of simulation, the speed slowly goes down to $0$ because of the inertia.

Note that the timing reported in the graphs are the ones of Plant Simulation, which is running at $47\times$ the real
speed, while the \texttt{FMU} is running at real time, so for example in the second recipe the actual time that the motor took for reaching a speed
value of $0$ is $30$ seconds.

The code that is generating the input signals is the following.
\begin{codeInput}{basic}{The Milling() method of the Milling machine}{MillingMachine}
MillingReady := false
wait Time_stop_position
wait Time_stop_position
wait Time_stop_position

print "Starting FMU"

if cmbRecipe.Value = 1
	print "Recipe 1 selected"
	OPCUA.setItemValue("Simulation Step",1.0)
	OPCUA.setItemValue("Restart",true);

	OPCUA.setItemValue("Gear",0.00);
	OPCUA.setItemValue("Brake",0.00);
	OPCUA.setItemValue("DoStep",true);
	stopuntil SimulationTime = 1.0 prio 1

	OPCUA.setItemValue("Gear",1.00);
	OPCUA.setItemValue("Brake",0.00);
	OPCUA.setItemValue("DoStep",true);
	stopuntil SimulationTime = 2.0 prio 1

	OPCUA.setItemValue("Gear",1.00);
	OPCUA.setItemValue("Brake",0.00);
	OPCUA.setItemValue("DoStep",true);
	stopuntil SimulationTime = 3.0 prio 1

	OPCUA.setItemValue("Gear",0.00);
	OPCUA.setItemValue("Brake",0.50);
	OPCUA.setItemValue("DoStep",true);
	stopuntil SimulationTime = 4.0 prio 1

	OPCUA.setItemValue("Gear",0.00);
	OPCUA.setItemValue("Brake",0.00);
	OPCUA.setItemValue("DoStep",true);
	stopuntil SimulationTime = 5.0 prio 1

	OPCUA.setItemValue("Gear",0.00);
	OPCUA.setItemValue("Brake",0.00);
	OPCUA.setItemValue("DoStep",true);
	stopuntil SimulationTime = 6.0 prio 1

	OPCUA.setItemValue("Gear",0.00);
	OPCUA.setItemValue("Brake",0.00);
	OPCUA.setItemValue("DoStep",true);
	stopuntil SimulationTime = 7.0 prio 1

	OPCUA.setItemValue("Gear",0.00);
	OPCUA.setItemValue("Brake",0.00);
	OPCUA.setItemValue("DoStep",true);
	stopuntil SimulationTime = 8.0 prio 1

	OPCUA.setItemValue("Gear",0.00);
	OPCUA.setItemValue("Brake",0.00);
	OPCUA.setItemValue("DoStep",true);
	stopuntil SimulationTime = 9.0 prio 1

	OPCUA.setItemValue("Gear",0.00);
	OPCUA.setItemValue("Brake",10.00);
	OPCUA.setItemValue("DoStep",true);
	stopuntil SimulationTime = 10.0 prio 1
elseif cmbRecipe.Value = 2
	print "Recipe 2 selected"
	OPCUA.setItemValue("Simulation Step",1.0)
	OPCUA.setItemValue("Restart",true);

	OPCUA.setItemValue("Gear",0.00);
	OPCUA.setItemValue("Brake",0.00);
	OPCUA.setItemValue("DoStep",true);
	stopuntil SimulationTime = 1.0 prio 1

	OPCUA.setItemValue("Gear",1.00);
	OPCUA.setItemValue("Brake",0.00);
	OPCUA.setItemValue("DoStep",true);
	stopuntil SimulationTime = 2.0 prio 1

	OPCUA.setItemValue("Gear",2.00);
	OPCUA.setItemValue("Brake",0.00);
	OPCUA.setItemValue("DoStep",true);
	stopuntil SimulationTime = 3.0 prio 1

	OPCUA.setItemValue("Gear",0.00);
	OPCUA.setItemValue("Brake",0.30);
	OPCUA.setItemValue("DoStep",true);
	stopuntil SimulationTime = 4.0 prio 1

	OPCUA.setItemValue("Gear",0.00);
	OPCUA.setItemValue("Brake",0.30);
	OPCUA.setItemValue("DoStep",true);
	stopuntil SimulationTime = 5.0 prio 1
//elseif cmbRecipe.Value = 3 and so on...
end

while SpeedOutput /= 0.0
	OPCUA.setItemValue("DoStep",true);
	Chart.update
	var olds := SimulationTime
	waituntil SimulationTime = olds + 1
end

print "Milling and now waiting"
wait SimulationTime

MillingReady := true
\end{codeInput}
