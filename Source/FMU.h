#ifndef FMU2OPCUA_FMU_H
#define FMU2OPCUA_FMU_H

#include "fmi4cpp/fmi4cpp.hpp"
#include <any>
#include <unordered_map>

using namespace fmi4cpp;

/**
 * This class can simulate a generic FMU, keeping all its I/O ports values updated in the respective maps
 */
class FMU {
public:
  /**
   * This enum represents the current state of the FMU simulation
   */
  enum FMU_STATE { FMU_RESET, FMU_RUNNING, FMU_SIMENDED };

  /**
   * Class constructor
   * @param path Path to the FMU
   */
  explicit FMU(const std::string &path);

  /**
   * Class destructor
   */
  ~FMU();

  /**
   * The main simulation method
   * @note It's expected to be called inside a separate thread
   */
  [[noreturn]] void simulate();

  /**
   * Reset and restart the simulation
   */
  void restartSimulation();

  /**
   * @return True if simulation has ended
   */
  bool isSimulating();

  /**
   * Set the sleep time between every simulation cycle
   * @param time Time in milliseconds
   */
  void setSimulationSleep(unsigned int time);

  /**
   * @return Returns the current simulation time
   */
  double getSimulationTime();

  /**
   * This map holds all the FMU exposed input ports, using the value reference as key
   */
  std::unordered_map<fmi2ValueReference,
                     fmi2::scalar_variable>
      input_ports;

  /**
   * This map holds all the FMU exposed output ports, using the value reference as key
   */
  std::unordered_map<fmi2ValueReference,
                     fmi2::scalar_variable>
      output_ports;

  /**
   * This map holds all the FMU exposed input ports' values, using the value reference as key
   */
  std::unordered_map<fmi2ValueReference, std::any>
      input_values;

  /**
   * This map holds all the FMU exposed output ports' values, using the value reference as key
   */
  std::unordered_map<fmi2ValueReference, std::any>
      output_values;

  /**
   * If this flag is true, the simulation never ends
   */
  bool infinite_simulation = false;

private:
  std::shared_ptr<fmi2::cs_fmu> fmu_cs;
  std::shared_ptr<const fmi2::cs_model_description> fmu_md;
  std::shared_ptr<fmi2::cs_slave> fmu_instance;

  void
  updateInputs();// this method is called internally by simulate() and just updates the input values

  void
  updateOutputs();// this method is called internally by simulate() and just updates the output values

  FMU_STATE fmu_state = FMU_SIMENDED;

  // these values control the simulation settings of the FMU
  // they are initialized in the class constructor using the default values
  double step_size  = 1.0;
  double start_time = 0.0;
  double stop_time  = 100.0;
  double tolerance  = 0.01;

  unsigned int sim_sleep_ms =
      100;// time in milliseconds for the simulation cycle
};

#endif//FMU2OPCUA_FMU_H
