#ifndef FMU2OPCUA_FMU2OPCUA_H
#define FMU2OPCUA_FMU2OPCUA_H

#define LANG const_cast<char *>("en-US")

#include "FMU.h"
#include "open62541.h"
#include <string>

class FMU2OPCUA {
public:
  explicit FMU2OPCUA(UA_Server *server, FMU *fmu);

  ~FMU2OPCUA();

  void updateVariables();

  void addVariableReal(const std::string &name,
                       const std::string &description,
                       const std::string &nodeid);

  void addVariableInteger(const std::string &name,
                          const std::string &description,
                          const std::string &nodeid);

  void addVariableBoolean(const std::string &name,
                          const std::string &description,
                          const std::string &nodeid);

  void addVariableString(const std::string &name,
                         const std::string &description,
                         const std::string &nodeid);

  void writeVariableReal(const std::string &nodeid,
                         UA_Double value);

  void writeVariableInteger(const std::string &nodeid,
                            UA_Int32 value);

  void writeVariableBoolean(const std::string &nodeid,
                            UA_Boolean value);

  void writeVariableString(const std::string &nodeid,
                           UA_String value);

  UA_Double readVariableReal(const std::string &nodeid);

  UA_Int32 readVariableInteger(const std::string &nodeid);

  UA_Boolean readVariableBoolean(const std::string &nodeid);

  UA_String readVariableString(const std::string &nodeid);

private:
  UA_Server *server;
  FMU *fmu;
};

#endif//FMU2OPCUA_FMU2OPCUA_H
