#ifndef AMLPARSER_AML2MATLAB_H
#define AMLPARSER_AML2MATLAB_H

#include "AMLDOMParser.h"
#include "nlohmann/json.hpp"
#include <unordered_map>

/**
 * This class is capable of converting an AML
 * InstanceHierarchy in a MATLAB script, which automatically
 * builds a Simulink model and exports a standalone FMU
 */
class AML2MATLAB {
public:
  /**
   * Class constructor
   * @param p Instance of the AMLDOMParser, please note that
   * the document has to parsed before using this class
   * @param path Path to save the generated MATLAB script
   */
  AML2MATLAB(AMLDOMParser *p, const std::string &path);

  /**
   * Class destructor
   */
  ~AML2MATLAB();

  /**
   * Performs the actual MATLAB conversion
   */
  void convertToMATLAB();

private:
  AMLDOMParser *parser;
  std::ofstream script;
  std::string modelName;

  std::ifstream simulinkPaths;
  std::ifstream simulinkAttributeValues;
  std::ifstream simulinkAttributes;
  nlohmann::json simulinkPathsJSON;
  nlohmann::json simulinkAttributesJSON;
  nlohmann::json simulinkAttributeValuesJSON;

  /**
   * Write a brief header in the MATLAB script
   */
  void writeHeaderComment();

  /**
   * Write the model simulation parameters
   * @note For now, they are hardcoded, though they could be
   * written in the AML file
   */
  void writeModelParams(const InternalElement &block);

  /**
   * Generate and write an add_block() MATLAB instruction
   * @param block InternalElement to convert
   */
  void writeAddBlock(const InternalElement &block);

  /**
   * Generate and write an add_line() MATLAB instruction
   * @param block InternalElement containing the
   * InternalLinks to convert
   */
  void writeAddLine(const InternalElement &block);

  /**
   * Write the instruction for arranging the Simulink model
   * and to export it as an FMU
   */
  void writeEnd();

  /**
   * Converts a RoleClassLib path to a real Simulink one
   * @param path AML's RoleClassLib path
   * @return Correspondent Simulink path
   */
  std::string getSimulinkPath(const XercesString &path);

  /**
   * Convert a specific block attribute's name in the real
   * Simulink one Like for example the "Torque Effective
   * Radius" attribute of a Simscape Disk Friction Clutch
   * becomes "r_eff" in the MATLAB code
   * @param path AML RoleClassLib path
   * @param attributeName The attribute tag to convert
   * @note This method assumes that a Simulink block doesn't
   * have two identical attribute name identifiers
   * @return Correspondent Simulink block attributeName
   */
  std::string getSimulinkAttributeName(
      const XercesString &path,
      const XercesString &attributeName);

  /**
   * Convert a specific block attribute's value identifier
   * in the real Simulink one Like for example the
   * "Bidirectional" attribute of a Simscape Simple Gear
   * becomes "0" in the MATLAB code
   * @param path AML RoleClassLib path
   * @param attributeValue The attribute tag to convert
   * @note This method assumes that a Simulink block doesn't
   * have two identical value identifiers
   * @return Correspondent Simulink block attribute value
   */
  std::string getSimulinkAttributeValue(
      const XercesString &path,
      const XercesString &attributeValue);
};

#endif // AMLPARSER_AML2MATLAB_H
