%===============================================================
% The TwoSpeedTransmission model
% This script was generated automatically
% by the AMLparser (https://gitlab.com/JackHack96/amlparser)
%===============================================================

model = 'TwoSpeedTransmission';
new_system(model);
open_system(model);
set_param(model,'FixedStep','auto');
set_param(model,'SolverType','Fixed-step');
set_param(model,'StartTime','0.0');
set_param(model,'StopTime','10.0');
add_block('sdl_lib/Clutches/Disk Friction Clutch',
    [model, '/Clutch Brake'],...
    'unidirectional', '0',...
    'r_eff', '100',...
    'piston_area', '0.001',...
    'geometry_model', '1',...
    'nS', '4');
add_block('built-in/Constant',
    [model, '/Clutches High Gear'],...
    'Value', '[0 1]');
%===============================================================
% ALL THE OTHER add_block() HERE
%===============================================================

add_line(model,
    'Simulink-PS Converter2/RConn1',
    'Clutch Low Gear/LConn1');
add_line(model,
    'Solver Configuration/RConn1',
    'MRRef Source/LConn1');
%===============================================================
% ALL THE OTHER add_line() HERE
%===============================================================

Simulink.BlockDiagram.arrangeSystem(model);
exportToFMU2CS(model, ...
    'CreateModelAfterGeneratingFMU', 'off', ...
    'AddIcon', 'snapshot', ...
    'SaveDirectory', pwd);